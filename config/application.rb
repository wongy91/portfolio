require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Jackportfolio
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true
    config.assets.initialize_on_precompile = false

    config.action_mailer.delivery_method = :smtp
    # SMTP settings for gmail
    config.action_mailer.smtp_settings = {
     :address              => "smtp.mailgun.org",
     :port                 => 587,
     :domain               => ENV["MAILGUN_DOMAIN"] || "sandboxd7926440eeb9402a9da1b6954c93784a.mailgun.org",
     :user_name            => ENV["MAILGUN_USERNAME"] || "postmaster@sandboxd7926440eeb9402a9da1b6954c93784a.mailgun.org",
     :password             => ENV["MAILGUN_PASSWORD"] || "093032751e0d76cb82f40cf2c6fefdb7",
     :authentication       => "plain",
     :enable_starttls_auto => true
    }
  end
end
