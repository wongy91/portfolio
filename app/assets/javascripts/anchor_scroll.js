$(function() {
  $(".masthead-nav a.navi").on("click", function(event) {
    if (this.hash !== "") {
      event.preventDefault();
      var hash = this.hash;
      clearActive();

      $(this.closest("li")).addClass("active");

      $("html, body").animate({
        scrollTop: $(hash).offset().top
      }, 300, function() {
        window.location.hash = hash;
      });
    }
  });

  function clearActive() {
    $(".masthead-nav li").removeClass("active");
  }
})
