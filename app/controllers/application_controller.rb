class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def download_resume
    file = "#{Rails.root}/public/resume_jack_wong.pdf"
    if File.exists?(file)
      send_file file, :type=>"application/pdf", :x_sendfile=>true
    else
      flash[:notice] = 'File not found'
      redirect_to :index
    end
  end

  def contact_me
    ContactJackMailer.contact_jack_email(contact_me_params).deliver_now

    respond_to do |format|
      format.js do
        render "contact_jack_mailer/email_sent"
      end
    end
  end

  def contact_me_params
    params.require(:contact_me).permit(
      :name,
      :from_email,
      :message
    )
  end
end
