class ContactJackMailer < ApplicationMailer
  def contact_jack_email(email_params)
    @email_params = email_params

    mail(
      to: "jackyhwong91@gmail.com",
      subject: "#{@email_params[:name]} contacted you from your portfolio. @ www.jackwong.win",
      from: @email_params[:from_email]
    )
  end
end
